CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE tag(
    id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    title VARCHAR(50) NOT NULL,
    description TEXT NOT NULL,
);

CREATE TABLE post(
    id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    title VARCHAR(50) NOT NULL,
    description TEXT NOT NULL,
    tag VARCHAR(50) NOT NULL
);

CREATE TABLE person(
    id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    gender VARCHAR(7) NOT NULL,
    age INTEGER,
    UNIQUE(email)
);

CREATE TABLE person_post(
    id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    personId UUID REFERENCES person(id) ,
    postIds UUID REFERENCES post(id)
);

CREATE TABLE tag_post(
    id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    tagId UUID REFERENCES tag(id),
    postId UUID REFERENCES postId()
);


INSERT INTO tag (title, description)VALUES('Jokes', 'This tag applies to stories only');
INSERT INTO tag (title, description)VALUES('Story', 'This tag applies to jokes only');
INSERT INTO tag (title, description)VALUES('Poem', 'This tag applies to poems only');

INSERT INTO post (title, description, tag)VALUES('1st title', 'This is my 1st post', 'Jokes');
INSERT INTO post (title, description, tag)VALUES('2nd title', 'This is my 2nd post', 'Jokes');
INSERT INTO post (title, description, tag)VALUES('3rd title', 'This is my 3rd post', 'Jokes');
INSERT INTO post (title, description, tag)VALUES('4th title', 'This is my 4th post', 'Jokes');
INSERT INTO post (title, description, tag)VALUES('5th title', 'This is my 5th post', 'Jokes');
INSERT INTO post (title, description, tag)VALUES('6th title', 'This is my 6th post', 'Jokes');
INSERT INTO post (title, description, tag)VALUES('7th title', 'This is my 7th post', 'Jokes');
INSERT INTO post (title, description, tag)VALUES('8th title', 'This is my 8th post', 'Jokes');
INSERT INTO post (title, description, tag)VALUES('9th title', 'This is my 9th post', 'Jokes');
INSERT INTO post (title, description, tag)VALUES('10th title', 'This is my 10th post', 'Jokes');

INSERT INTO person (name, email, password, gender, age)VALUES('Snahashis Das', 'snahashis@gmail.com', 'test1234', 'Male', 26);
INSERT INTO person (name, email, password, gender, age)VALUES('Sayan Das', 'sayan@gmail.com', 'test1234', 'Male', 23);
INSERT INTO person (name, email, password, gender, age)VALUES('Ayanava Paul', 'ayanava@gmail.com', 'test1234', 'Male', 27);
INSERT INTO person (name, email, password, gender, age)VALUES('Avijit Sarkar', 'avijit@gmail.com', 'test1234', 'Male', 23);
INSERT INTO person (name, email, password, gender, age)VALUES('Musabbar Hossain', 'musabbar@gmail.com', 'test1234', 'Male', 26);
INSERT INTO person (name, email, password, gender, age)VALUES('Prithvi Ghosh', 'prithvi@gmail.com', 'test1234', 'Male', 27);
INSERT INTO person (name, email, password, gender, age)VALUES('Ritam Chatterjee', 'ritam@gmail.com', 'test1234', 'Male', 22);
INSERT INTO person (name, email, password, gender, age)VALUES('Rohit Das', 'rohit@gmail.com', 'test1234', 'Male', 26);
INSERT INTO person (name, email, password, gender, age)VALUES('Rohit Das', 'rohit2@gmail.com', 'test1234', 'Male', 23);
INSERT INTO person (name, email, password, gender, age)VALUES('Saurav Daniel', 'saurav@gmail.com', 'test1234', 'Male', 27);
INSERT INTO person (name, email, password, gender, age)VALUES('Priyanka Das', 'priyanka954@gmail.com', 'test1234','Female', 20);
INSERT INTO person (name, email, password, gender, age)VALUES('Mou Roy', 'mou@gmail.com', 'test1234','Female', 21);
INSERT INTO person (name, email, password, gender, age)VALUES('Dipanwita Jana', 'dipanwita4@gmail.com', 'tet1234','Female', 24);
INSERT INTO person (name, email, password, gender, age)VALUES('Ipshita Sannal', 'ipshita@gmail.com', 'test1234','Female, 25');
INSERT INTO person (name, email, password, gender, age)VALUES('Ruhi Khatun', 'ruhi@gmail.com', 'test1234','Female', 23);
INSERT INTO person (name, email, password, gender, age)VALUES('Sushmita Sen', 'sushmia@gmail.com', 'test1234','Female', 22);
INSERT INTO person (name, email, password, gender, age)VALUES('Manashi Bagchi', 'manashi@gmail.com', 'test1234','Female', 23);
INSERT INTO person (name, email, password, gender, age)VALUES('Amrita Sarkar', 'amrita@gmail.com', 'test1234','Female', 25);
INSERT INTO person (name, email, password, gender, age)VALUES('Dipshikha Thakur', 'dipshikha@gmail.com', 'test1234','Female', 22);

INSERT INTO person_post(personId, postIds)
    VALUES('', '');
INSERT INTO person_post(personId, postIds)
    VALUES('', '');

INSERT INTO tag_post(personId, postIds)
    VALUES('', '');
INSERT INTO tag_post(personId, postIds)
    VALUES('', '');

SELECT personId, COUNT(*) from person_post GROUP BY personId;

SELECT * FROM person_post
    JOIN person ON person_post.personId = person.id
    JOIN post ON person_post.postIds = post.id;

SELECT COUNT(*) FROM person_post
    JOIN person ON person_post.personId = person.id
    JOIN post ON person_post.postIds = post.id
        GROUP BY personId;
        

SELECT * FROM person;
