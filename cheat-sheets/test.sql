CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE person(
    id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    gender VARCHAR(7) NOT NULL,
    age INTEGER,
    UNIQUE(email)
);
CREATE TABLE post(
    id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    title VARCHAR(50) NOT NULL,
    description TEXT NOT NULL,
    tags UUID[]
); 
CREATE TABLE tag(
    id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    title VARCHAR(50) NOT NULL,
    description TEXT NOT NULL,
    posts UUID[]
);
ALTER TABLE tag ADD CONSTRAINT fk_tags FOREIGN KEY (EACH ELEMENT OF posts) REFERENCES tag_post(id);
ALTER TABLE post ADD CONSTRAINT fk_posts FOREIGN KEY (EACH ELEMENT OF tags) REFERENCES tag_post(id);

CREATE TABLE person_post(
    id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    personId UUID,
    postId UUI
);
ALTER TABLE person_post ADD CONSTRAINT fk_person_id FOREIGN KEY (personId) REFERENCES person(id),
ALTER TABLE person_post ADD CONSTRAINT fk_post_id FOREIGN KEY (postId) REFERENCES post(id);

CREATE TABLE tag_post(
    id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    tagId UUID, 
    postId UUID
);
ALTER TABLE tag_post ADD CONSTRAINT fk_tag_id FOREIGN KEY (tagId) REFERENCES tag(id);
ALTER TABLE tag_post ADD CONSTRAINT fk_post_id FOREIGN KEY (postId) REFERENCES post(id);