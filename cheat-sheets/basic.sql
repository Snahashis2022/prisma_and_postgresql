[PostgreSQL Basics]

SELECT * FROM person WHERE name='Rohit Das' AND (gender='Male' OR id=9);
SELECT * FROM person WHERE name IN('Snahashis Das', 'Sayan Das');
SELECT * FROM person ORDER BY id ASC;
SELECT * FROM person OFFSET 2 LIMIT 4;
SELECT * FROM person OFFSET 2 FETCH FIRST 3 ROW ONLY;
SELECT * FROM person WHERE name='Snahashis Das' OR name='Saurav Daniel' 
SELECT * FROM person WHERE id BETWEEN 3 AND 7;
SELECT * FROM person WHERE name LIKE '%Das';
SELECT * FROM person WHERE name LIKE 'S%';
SELECT * FROM person WHERE email LIKE '_____@%';
SELECT name, COUNT(*) from person GROUP BY name;
SELECT gender, COUNT(*) from person GROUP BY gender;
SELECT gender, COUNT(*) from person GROUP BY gender HAVING COUNT(*)>5;
INSERT INTO person (name, email, password, gender)VALUES('Dipshikha Thakur', 'dipshikha@gmail.com', 'test1234','Female');
ALTER TABLE person ADD age INTEGER; (Adds a new column to the person table);
UPDATE person SET age=25 WHERE email LIKE '______@%';
SELECT MAX(age) FROM person;
INSERT INTO person (name, email, password, gender, age)VALUES('Snahashis Das', 'snahashis@gmail.com', 'test1234', 'Male', 25);











