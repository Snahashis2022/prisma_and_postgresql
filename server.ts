import express, {Request, Response} from 'express';

const app = express();
const PORT = 3000; 

app.get("/", (req:Request, res:Response)=>{
	return res.json({data:"Hello"});
});

app.listen(PORT, ()=>{
console.log(`Server started on ${PORT}`)
;});

